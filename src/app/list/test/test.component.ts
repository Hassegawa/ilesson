import { Component, OnInit } from '@angular/core';
import { DataService, Item } from 'src/app/data.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {

  constructor(private data: DataService) {

  }

  public selected: Item = new Item();
  public simplePast: string;
  public pastParticiple: string;
  public errorSimplePast: boolean;
  public errorPastParticiple: boolean;
  public iconS = 'help';
  public iconP = 'help';

  ngOnInit() {
    this.next();
  }

  public next() {
    this.simplePast = '';
    this.pastParticiple = '';
    this.errorPastParticiple = false;
    this.errorSimplePast = false;
    this.iconS = 'help';
    this.iconP = 'help';

    this.data.GetRandomItem().then(item => {
      this.selected = item;
    });
  }

  public check() {
    if (this.selected.SimplePast.toLowerCase() === this.simplePast.toLowerCase()) {
      this.iconS = 'done-all';
    } else {
      this.iconS = 'close';
      this.errorSimplePast = true;
    }

    if (this.selected.PastParticiple.toLowerCase() === this.pastParticiple.toLowerCase()) {
      this.iconP = 'done-all';
    } else {
      this.iconP = 'close';
      this.errorPastParticiple = true;
    }
  }

}
