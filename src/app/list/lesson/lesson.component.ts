import { Component, OnInit } from '@angular/core';
import { DataService, Item } from 'src/app/data.service';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss'],
})
export class LessonComponent implements OnInit {

  constructor(private data: DataService) {

  }

  public list: Array<Item> = new Array<Item>();
  public selected: Item;
  public simplePast: string;
  public pastParticiple: string;
  public errorSimplePast: boolean;
  public errorPastParticiple: boolean;
  public iconS = 'help';
  public iconP = 'help';
  private count = 0;
  private size = 5;

  ngOnInit() {
    this.data.GetLessonItems(this.size).then(items => {
      this.list = items;
    })

    this.next();
    this.selected = this.list[this.count];
  }

  public next() {
    this.simplePast = '';
    this.pastParticiple = '';
    this.errorPastParticiple = false;
    this.errorSimplePast = false;
    this.iconS = 'help';
    this.iconP = 'help';

    if (this.count >= (this.count)){
      this.count = 0;
    }

    this.selected = this.list[this.count++];
  }

  public check() {
    if (this.selected.SimplePast.toLowerCase() === this.simplePast.toLowerCase()) {
      this.iconS = 'done-all';
    } else {
      this.iconS = 'close';
      this.errorSimplePast = true;
    }

    if (this.selected.PastParticiple.toLowerCase() === this.pastParticiple.toLowerCase()) {
      this.iconP = 'done-all';
    } else {
      this.iconP = 'close';
      this.errorPastParticiple = true;
    }
  }

}
