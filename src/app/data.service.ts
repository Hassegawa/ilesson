import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private items: Array<Item> = [];
  private fasleCognates: Array<FalseCongnate> = [];

  public GetList(): Promise<Array<Item>> {
    return new Promise((success) => {
      success(this.items);
    });
  }

  public GetRandomItem(): Promise<Item>{
    return new Promise((success) => {
      const rnum = Math.floor(Math.random() * this.items.length) - 1;
      success(this.items[rnum]);
    });
  }

  public GetLessonItems(size: number): Promise<Array<Item>>{
    return new Promise((success) => {
      const rnum = Math.floor(Math.random() * this.items.length) - 1;
      success(this.items.slice(1,size));
    });
  }

  constructor() {

    this.items =
    [
      { Infinitive : 'Be' , SimplePast : 'Was/were', PastParticiple : 'Been', Significado : 'Ser' },
      { Infinitive : 'Bear' , SimplePast : 'Bore', PastParticiple : 'Born/Borne', Significado : 'Suportar' },
      { Infinitive : 'Beat' , SimplePast : 'Beat', PastParticiple : 'Beaten', Significado : 'Bater/vencer' },
      { Infinitive : 'Become' , SimplePast : 'Became', PastParticiple : 'Become', Significado : 'Tornar-se' },
      { Infinitive : 'Begin' , SimplePast : 'Began', PastParticiple : 'Begun', Significado : 'Começar' },
      { Infinitive : 'Bite' , SimplePast : 'Bit', PastParticiple : 'Bitten', Significado : 'Morder' },
      { Infinitive : 'Bleed' , SimplePast : 'Bled', PastParticiple : 'Bled', Significado : 'Sangrar' },
      { Infinitive : 'Break' , SimplePast : 'Broke', PastParticiple : 'Broken', Significado : 'Quebrar' },
      { Infinitive : 'Build' , SimplePast : 'Built', PastParticiple : 'Built', Significado : 'Construir' },
      { Infinitive : 'Catch' , SimplePast : 'Caught', PastParticiple : 'Caught', Significado : 'Pegar/capturar' },
      { Infinitive : 'Choose' , SimplePast : 'Chose', PastParticiple : 'Chosen', Significado : 'Escolher' },
      { Infinitive : 'Cling' , SimplePast : 'Clung', PastParticiple : 'Clung', Significado : 'Unir-se' },
      { Infinitive : 'Deal' , SimplePast : 'Dealt', PastParticiple : 'Dealt', Significado : 'Negociar/lidar/dar as cartas' },
      { Infinitive : 'Dig' , SimplePast : 'Dug', PastParticiple : 'Dug', Significado : 'Cavar' },
      { Infinitive : 'Do' , SimplePast : 'Did', PastParticiple : 'Done', Significado : 'Fazer' },
      { Infinitive : 'Draw' , SimplePast : 'Drew', PastParticiple : 'Drawn', Significado : 'Desenhar/traçar' },
      { Infinitive : 'Drink' , SimplePast : 'Drank', PastParticiple : 'Drunk', Significado : 'Beber' },
      { Infinitive : 'Drive' , SimplePast : 'Drove', PastParticiple : 'Driven', Significado : 'Dirigir/guiar' },
      { Infinitive : 'Eat' , SimplePast : 'Ate', PastParticiple : 'Eaten', Significado : 'Comer' },
      { Infinitive : 'Fall' , SimplePast : 'Fell', PastParticiple : 'Fallen', Significado : 'Cair' },
      { Infinitive : 'Feel' , SimplePast : 'Felt', PastParticiple : 'Felt', Significado : 'Sentir' },
      { Infinitive : 'Fight' , SimplePast : 'Fought', PastParticiple : 'Fought', Significado : 'Lutar' },
      { Infinitive : 'Find' , SimplePast : 'Found', PastParticiple : 'Found', Significado : 'Encontrar/descobrir' },
      { Infinitive : 'Fly' , SimplePast : 'Flew', PastParticiple : 'Flown', Significado : 'Voar' },
      { Infinitive : 'Forbid' , SimplePast : 'Forbade', PastParticiple : 'Forbidden', Significado : 'Proibir/impedir' },
      { Infinitive : 'Forget' , SimplePast : 'Forgot', PastParticiple : 'Forgotten', Significado : 'Esquecer' },
      { Infinitive : 'Forgive' , SimplePast : 'Forgave', PastParticiple : 'Forgiven', Significado : 'Perdoar' },
      { Infinitive : 'Give' , SimplePast : 'Gave', PastParticiple : 'Given', Significado : 'Dar' },
      { Infinitive : 'Have' , SimplePast : 'Had', PastParticiple : 'Had', Significado : 'Ter/possuir' },
      { Infinitive : 'Hide' , SimplePast : 'Hid', PastParticiple : 'Hidden', Significado : 'Ocultar/esconder' },
      { Infinitive : 'Hold' , SimplePast : 'Held', PastParticiple : 'Held', Significado : 'Segurar/aguardar' },
      { Infinitive : 'Kneel' , SimplePast : 'Knelt', PastParticiple : 'Knelt', Significado : 'Ajoelhar-se' },
      { Infinitive : 'Know' , SimplePast : 'Knew', PastParticiple : 'Known', Significado : 'Saber' },
      { Infinitive : 'Lay' , SimplePast : 'Laid', PastParticiple : 'Laid', Significado : 'Colocar' },
      { Infinitive : 'Leave' , SimplePast : 'Left', PastParticiple : 'Left', Significado : 'Deixar/ir embora' },
      { Infinitive : 'Lend' , SimplePast : 'Lent', PastParticiple : 'Lent', Significado : 'Emprestar' },
      { Infinitive : 'Lose' , SimplePast : 'Lost', PastParticiple : 'Lost', Significado : 'Perder' },
      { Infinitive : 'Make' , SimplePast : 'Made', PastParticiple : 'Made', Significado : 'Fazer/criar' },
      { Infinitive : 'Meet' , SimplePast : 'Met', PastParticiple : 'Met', Significado : 'Conhecer' },
      { Infinitive : 'Pay' , SimplePast : 'Paid', PastParticiple : 'Paid', Significado : 'Pagar' },
      { Infinitive : 'Ride' , SimplePast : 'Rode', PastParticiple : 'Ridden', Significado : 'Andar de bicicleta/moto/a cavalo' },
      { Infinitive : 'Ring' , SimplePast : 'Rang', PastParticiple : 'Rung', Significado : 'Tocar sino ou telefone/ligar' },
      { Infinitive : 'Run' , SimplePast : 'Ran', PastParticiple : 'Run', Significado : 'Correr' },
      { Infinitive : 'Say' , SimplePast : 'Said', PastParticiple : 'Said', Significado : 'Dizer' },
      { Infinitive : 'See' , SimplePast : 'Saw', PastParticiple : 'Seen', Significado : 'Ver/olhar' },
      { Infinitive : 'Seek' , SimplePast : 'Sought', PastParticiple : 'Sought', Significado : 'Procurar/buscar' },
      { Infinitive : 'Send' , SimplePast : 'Sent', PastParticiple : 'Sent', Significado : 'Enviar' },
      { Infinitive : 'Shake' , SimplePast : 'Shook', PastParticiple : 'Shaken', Significado : 'Sacudir' },
      { Infinitive : 'Show' , SimplePast : 'Shower', PastParticiple : 'Shown', Significado : 'Mostrar/aparecer' },
      { Infinitive : 'Sit' , SimplePast : 'Sat', PastParticiple : 'Sat', Significado : 'Sentar-se' },
      { Infinitive : 'Sow' , SimplePast : 'Sowed', PastParticiple : 'Sown', Significado : 'Semear/espalhar' },
      { Infinitive : 'Spell' , SimplePast : 'Spelt', PastParticiple : 'Spelt', Significado : 'Soletrar' },
      { Infinitive : 'Stand' , SimplePast : 'Stood', PastParticiple : 'Stood', Significado : 'Ficar de pé' },
      { Infinitive : 'Steal' , SimplePast : 'Stole', PastParticiple : 'Stolen', Significado : 'Roubar' },
      { Infinitive : 'Swim' , SimplePast : 'Swam', PastParticiple : 'Swum', Significado : 'Nadar' },
      { Infinitive : 'Take' , SimplePast : 'Took', PastParticiple : 'Taken', Significado : 'Pegar/tirar/levar' },
      { Infinitive : 'Throw' , SimplePast : 'Threw', PastParticiple : 'Thrown', Significado : 'Arremessar' },
      { Infinitive : 'Understand' , SimplePast : 'Understood', PastParticiple : 'Understood', Significado : 'Entender/compreender' },
      { Infinitive : 'Wear' , SimplePast : 'Wore', PastParticiple : 'Worn', Significado : 'Vestir/usar' },
      { Infinitive : 'Write' , SimplePast : 'Wrote', PastParticiple : 'Written', Significado : 'Escrever' }
    ];

    this.fasleCognates = [ { English : 'Pretend', Portuguese : 'Fingir' },
      { English : 'Prejudice', Portuguese : 'Preconceito' },
      { English : 'College', Portuguese : 'Faculdade' },
      { English : 'Library', Portuguese : 'Biblioteca' },
      { English : 'Support', Portuguese : 'Apoiar' },
      { English : 'Intend', Portuguese : 'Pretender' },
      { English : 'Lunch', Portuguese : 'Almoço' },
      { English : 'Devolve', Portuguese : 'Transferir' },
      { English : 'Mayor', Portuguese : 'Prefeito' },
      { English : 'Anthem', Portuguese : 'Hino' },
      { English : 'Parents', Portuguese : 'Pais (mãe e pai)' },
      { English : 'Costum', Portuguese : 'Fantasia' },
      { English : 'Eventually', Portuguese : 'Finalmente, eventualmente' },
      { English : 'Exit' , Portuguese : 'Saída' },
      { English : 'Fabric', Portuguese : 'Tecido' },
      { English : 'Lecture', Portuguese : 'Palestra' },
      { English : 'Novel', Portuguese : 'Romance' },
      { English : 'Application', Portuguese : 'Inscrição' },
      { English : 'Attend', Portuguese : 'Assistir, Participar' },
      { English : 'Pasta', Portuguese : 'Massa (alimento)' },
      { English : 'Sensible', Portuguese : 'Sensato' },
      { English : 'Realize', Portuguese : 'Perceber' },
      { English : 'Shoot', Portuguese : 'Atirar, fotografar/filmar' },
      { English : 'Actually', Portuguese : 'Na verdade' },
      { English : 'Pull', Portuguese : 'Puxar' },
      { English : 'Baton', Portuguese : 'Cacetete' },
      { English : 'Enroll', Portuguese : 'Matricular-se, inscrever-se' },
      { English : 'Push', Portuguese : 'Empurrar' },
      { English : 'Convict', Portuguese : 'Condenado' },
      { English : 'Tax', Portuguese : 'Imposto' },
      { English : 'Coroner', Portuguese : 'Legista' },
      { English : 'Patron', Portuguese : 'Cliente' },
      { English : 'Foosball', Portuguese : 'Pebolim' },
      { English : 'Genial', Portuguese : 'Agradável, amável' },
      { English : 'Valorous', Portuguese : 'Corajoso' } ]
  }
}

export class Item {
  public Infinitive: string;
  public SimplePast: string;
  public PastParticiple: string;
  public Significado: string;
}

export class FalseCongnate {
  public English: string;
  public Portuguese: string;
}
